def reverse_word(word: str) -> str:
    reversed_word = ""
    letters = [symbol for symbol in word if symbol.isalpha()]
    for symbol in word:
        reversed_word += letters.pop() if symbol.isalpha() else symbol
    return reversed_word


def get_special_revers(text: str) -> str:
    """Return reversed text:

    >>> get_special_revers('abcd')
    'dcba'

    the reverse for the text is implemented for individual words,
    the words remain innthe thair places
    >>> get_special_revers('abcd efgh')
    'dcba hgfe'

    if the word contains a non-letter character, it will be left in the
    same place:
    >>> get_special_revers('qw%^er @12ty')
    're%^wq @12yt'

    The function expects parameters with data type 'str'. When receiving input
    parameters of a different type, a TypeError exception is raised:
    >>> get_special_revers(1234)
    Traceback (most recent call last):
    ...
    TypeError: function get_special_revers() expects parameters of type 'str', but received type <class 'int'>

    """
    if not isinstance(text, str):
        raise TypeError(
            f"function get_special_revers() expects parameters of type 'str', but received type {type(text)}"
        )
    words = text.split()
    reversed_words = map(reverse_word, words)
    reversed_text = " ".join(reversed_words)
    return reversed_text


if __name__ == "__main__":
    import doctest

    doctest.testmod()
