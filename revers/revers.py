def reverse_word(word: str) -> str:
    reversed_word = ""
    letters = [symbol for symbol in word if symbol.isalpha()]
    for symbol in word:
        reversed_word += letters.pop() if symbol.isalpha() else symbol
    return reversed_word


def get_special_revers(text: str) -> str:
    words = text.split()
    reversed_words = map(reverse_word, words)
    reversed_text = " ".join(reversed_words)
    return reversed_text


if __name__ == "__main__":
    cases = (
        ("", ""),
        ("abcd", "dcba"),
        ("abcd efgh", "dcba hgfe"),
        ("ab1cd#", "dc1ba#"),
        ("qw%^er @12ty", "re%^wq @12yt"),
    )
    for num, (text, expected_reversed_text) in enumerate(cases):
        assert get_special_revers(text) == expected_reversed_text, (
            f"ERRON in case {num}: expected - get_special_revers('{text}') == '{expected_reversed_text}',"
            f"but got '{get_special_revers(text)}'"
        )
